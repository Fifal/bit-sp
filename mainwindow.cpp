#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this);

    connect(ui->btnGenerateKeys, SIGNAL(released()), this, SLOT(generateKeysHandler()));
    connect(ui->btnDecrypt, SIGNAL(released()), this, SLOT(decryptHandler()));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::generateKeysHandler() {
    if (ui->txtMsgToEncrypt->toPlainText() == "") {
        ui->log->append("Nejdříve musíte zadat zprávu k zašifrování!");
        return;
    }

    rsa r(ui);

    rsa::public_key pub_key;
    rsa::private_key priv_key;

    r.generate_keys(pub_key, priv_key);

    ui->log->append(
            util::get_char("Veřejný klíč: (" + std::to_string(pub_key.n) + ", " + std::to_string(pub_key.e) + ")"));
    ui->log->append(
            util::get_char("Privátní klíč: (" + std::to_string(priv_key.n) + ", " + std::to_string(priv_key.d) + ")"));

    std::string enc = r.encrypt(ui->txtMsgToEncrypt->toPlainText().toStdString(), pub_key);

    ui->txtMsgEncrypted->setText(enc.c_str());
    ui->txtMsgToDecrypt->setText(enc.c_str());

    ui->lineNumberN->setText(std::to_string(priv_key.n).c_str());
    ui->lineNumberD->setText(std::to_string(priv_key.d).c_str());
}

void MainWindow::decryptHandler() {
    if (ui->txtMsgToDecrypt->toPlainText() == "") {
        ui->log->append("Nejdříve musíte zadat zprávu k dešifrování!");
        return;
    }

    std::string number_n_str = ui->lineNumberN->text().toStdString();
    long number_n;
    try {
        number_n = stol(number_n_str);
    } catch (...) {
        ui->log->append("Text v políčko pro číslo n není číslo!");
        return;
    }

    std::string number_d_str = ui->lineNumberD->text().toStdString();
    long number_d;
    try {
        number_d = stol(number_d_str);
    } catch (...) {
        ui->log->append("Text v políčko pro číslo n není číslo!");
        return;
    }

    rsa::private_key priv_key;
    priv_key.n = number_n;
    priv_key.d = number_d;

    rsa r(ui);
    std::string dec = r.decrypt(ui->txtMsgToDecrypt->toPlainText().toStdString(), priv_key);

    ui->txtMsgDecrypted->setText(util::get_char(dec));
}
