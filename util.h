//
// Created by fifal on 2.4.17.
//

#ifndef BIT_SP_UTIL_H
#define BIT_SP_UTIL_H

#include <iostream>
#include <vector>

class util {
public:
    static std::vector<std::string> split(const std::string str, const std::string delim);
    static const char* get_char(std::string text);
};


#endif //BIT_SP_UTIL_H
