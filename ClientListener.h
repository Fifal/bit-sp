//
// Created by fifal on 13.4.17.
//

#ifndef BIT_SP_CLIENTLISTENER_H
#define BIT_SP_CLIENTLISTENER_H


#include "communicationwindow.h"
#include "network.h"

class ClientListener : public QThread {
    Q_OBJECT
    int m_socket;

signals:
    void message_received(QString info);

public:
    ClientListener(int socket);

    void run() override;
};


#endif //BIT_SP_CLIENTLISTENER_H
