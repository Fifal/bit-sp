/**
 * Třída poskytující metody pro zašifrování textu pomocí RSA asymetrického šifrování.
 *
 * Pro zašifrování textu je nutné nejdřív vygenerovat čísla p, q, d a n...
 *      to zajišťuje metoda generate_keys(), která jako parametry příjmá referenci na struktury
 *      veřejného a privátního klíče kam se vygenerované hodnoty uloží.
 *
 * Pak se zavolá metoda encrypt(), ta jako parametry příjmá text k zašifrování a veřejný klíč
 *      tato metoda text zašifruje po jednotlivých znacích a převede do HEX formátu,
 *      kde každý znak je oddělen mezerou.
 *
 * Odšifrování probíhá stejně jako šifrování, zavolá se metoda decrypt(), která příjmá zašifrovaný text
 *      a privátní klíč.
 */

#ifndef BIT_SP_CRYPTO_H
#define BIT_SP_CRYPTO_H

#include <iostream>
#include <future>
#include <climits>
#include "include/BigIntegerLibrary.hh"
#include <cmath>
#include <sstream>
#include "util.h"
#include "ui_mainwindow.h"

class rsa {
public:
    rsa(Ui::MainWindow* ui);
    rsa();

    /**
     * Struktura pro veřejný klíč
     */
    struct public_key {
        long n;
        long e;
    };

    /**
     * Struktura pro privátní klíč
     */
    struct private_key {
        long n;
        long d;
    };

    /**
     * Metoda pro zašifrování řetězce.
     * @param text - text k zašifrování
     * @param pub_key - struktura s hodnotami veřejného klíče
     * @return zašifrovaný řetězec
     */
    std::string encrypt(const std::string text, const struct public_key pub_key);

    /**
     * Metoda pro odšifrování řetězce.
     * @param encrypted - zašifrovaný text
     * @param priv_key - privátní klíč
     * @return odšifrovaný řetězec
     */
    std::string decrypt(const std::string encrypted, const struct private_key priv_key);

    /**
     * Metoda, která generuje privátní a veřejný klíč
     * @param pub_key - reference na strukturu pro veřejný klíč
     * @param priv_key - reference na strukturu pro privátní klíč
     */
    void generate_keys(struct public_key &pub_key, struct private_key &priv_key);

private:
    /** Uživatelské rozhraní aplikace */
    Ui::MainWindow* m_ui;

    /**
     * Metoda počítající funkci (p - 1) * (q - 1)
     * @param number_p číslo p
     * @param number_q číslo q
     * @return long hodnota funkce
     */
    long calc_euler(long &number_p, long &number_q);

    /**
     * Metoda, která hledá vhodné číslo e
     *      - vhodné číslo je:
     *           1) prvočíslo
     *           2) největší společný dělitel s hodnotou euler funkce je 1
     *
     * @param euler hodnota eulerovy funkce
     * @return long vhodné číslo e
     */
    long find_number_e(long &euler);

    /**
     * Metoda počítající číslo d
     *      - vrací pouze hodnotu funkce mod_inverse();
     *
     * @param euler eulerovo číslo
     * @param number_e číslo e
     * @return long číslo d
     */
    long find_number_d(long &euler, long &number_e);

    /**
     * Metoda vrací náhodné číslo z rozsahu <min; max>
     * @param min - minimální hodnota
     * @param max - maximální hodnota
     * @return long náhodné číslo
     */
    long get_random_number(long min, long max);

    /**
     * Vrací náhodné prvočíslo z rozsahu <min; max>
     * @param min - minimální hodnota
     * @param max - maximální hodnota
     * @return long náhodné prvočíslo
     */
    long get_random_prime(long min, long max);

    /**
     * Kontroluje zdali je číslo prvočíslo
     * @param number - číslo ke zkontrolování
     * @return true pokud se jedná o prvočíslo, false pokud ne
     */
    bool is_prime_number(long &number);

    /**
     * Vrací maximálního společného dělitele dvou čísel
     * @param a - první číslo
     * @param b - druhé číslo
     * @return long maximální společný dělitel
     */
    long gcd(long a, long b);

    /**
     * Metoda počítající číslo d
     * https://en.wikipedia.org/wiki/Modular_multiplicative_inverse
     * @param number_e - číslo e
     * @param number_euler - hodnota eulerovy funkceš
     * @return
     */
    long mod_inverse(long& number_e, long& number_euler);
};


#endif //BIT_SP_CRYPTO_H
